# 工具集

## Prettier

> Prettier 支持多种语言，它的一大特点就是能够支持命令行、API 等多种形式调用，可以让团队保持代码风格一致。包括 React 在内的很多项目已经开始使用了。

[Prettier ](https://prettier.io/) 是一个前端的代码格式化工具，支持列表如下：

- JavaScript，包括 ES2017
- JSX
- Flow
- TypeScript
- CSS, LESS, and SCSS
- JSON
- GraphQL

简而言之，这个工具能够使输出代码保持风格一致

### 使用方式

#### VS Code 插件

![mark](http://oly3dw73x.bkt.clouddn.com/blog/180412/9GG7cFI07b.png?imageslim)

#### npm

```JavaScript
npm install --save-dev --save-exact prettier

// or globally

npm install --global prettier
```

