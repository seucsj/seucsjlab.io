# HTML

## 通用规范

### 标签

- 自闭合（self-closing）标签，无需闭合 ( 例如： `img` `input` `br` `hr` 等 )；
- 可选的闭合标签（closing tag），需闭合 ( 例如：`</li>` 或 `</body>` )；
- 尽量减少标签数量；

```html
<img src="images/google.png" alt="Google">
<input type="text" name="title">

<ul>
  <li>Style</li>
  <li>Guide</li>
</ul>
```

### CSS 文件

在head中引入页面所需所有CSS文件

在页面渲染的过程中，新的CSS可能导致元素的样式重新计算和绘制，页面闪烁。

### JavaScript 文件

JavaScript 应当放在页面末尾，或采用异步加载。

将 `script` 放在页面中间将阻断页面的渲染。

```html
<body>
    <!-- a lot of elements -->
    <script src="init-behavior.js"></script>
</body>
```

### Class 和 ID

- class 应以功能或内容命名，不以表现形式命名；
- class 统一采用单词字母小写，多个单词组成时，采用中划线`-`分隔；
- id采用小驼峰命名法或全部小写字母，多个单词采用`-`分隔（**进一步确定统一？**）；

```html
<!-- Not recommended -->
<div class="left contentWrapper"></div>

<!-- Recommended -->
<div id="j-hook" class="sidebar content-wrapper"></div>
```

### 属性顺序

HTML 属性应该按照特定的顺序出现以保证易读性。

- id
- class
- name
- data-xxx
- src, for, type, href
- title, alt

```html
<a id="..." class="..." data-modal="toggle" href="##"></a>

<input class="form-control" type="text">

<img src="..." alt="...">
```

### 引号

属性的定义，统一使用双引号。

```html
<!-- Not recommended -->
<span id='j-hook' class=text>Google</span>

<!-- Recommended -->
<span id="j-hook" class="text">Google</span>
```

### 布尔值属性

HTML5 规范中 `disabled`、`checked`、`selected` 等属性不用设置值。

```html
<!-- Not recommended -->
<input type="text" disabled="disabled">

<!-- Recommended -->
<input type="text" disabled>

<input type="checkbox" value="1" checked>

<select>
  <option value="1" selected>1</option>
</select>
```

### 属性名必须使用小写字母

```html
<!-- Not recommended -->
<div data-cameraId="1">
  
</div>

<!-- Recommended -->
<div data-cameraid="1">
<div data-ui-type="Select">
```

### 省略type属性

因为HTML5在引入CSS时，默认type值为text/css；在引入JavaScript时，默认type值为text/javascript

```html
<!-- Not recommended -->
<link type="text/css" rel="stylesheet" href="base.css" />
<script type="text/javascript" src="base.js"></script>

<!-- Recommended -->
<link rel="stylesheet" href="base.css" />
<script src="base.js"></script>
```

### img标签要写alt属性

根据W3C标准，img标签要写alt属性，如果没有就写一个空的。但是一般要写一个有内容的，根据图片想要表达的意思，因为alt是在图片无法加载时显示的文字。

## HEAD

### 文档类型

为每个 HTML 页面的第一行添加标准模式（standard mode）的声明， 这样能够确保在每个浏览器中拥有一致的表现。

```html
<!DOCTYPE html>
```

### 字符编码

以无 BOM 的 utf-8 编码作为文件格式

```html
<html>
  <head>
    <meta charset="utf-8">
    ......
  </head>
  <body>
    ......
  </body>
</html>
```

### IE 兼容模式

优先使用最新版本的IE 和 Chrome 内核

```html
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
```

### SEO 优化

```html
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <!-- SEO -->
    <title>Style Guide</title>
    <meta name="keywords" content="your keywords">
    <meta name="description" content="your description">
    <meta name="author" content="author,email address">
</head>
```

### viewport

- `viewport`: 一般指的是浏览器窗口内容区的大小，不包含工具条、选项卡等内容；
- `width`: 浏览器宽度，输出设备中的页面可见区域宽度；
- `device-width`: 设备分辨率宽度，输出设备的屏幕可见宽度；
- `initial-scale`: 初始缩放比例；
- `maximum-scale`: 最大缩放比例；

为移动端设备优化，设置可见区域的宽度和初始缩放比例。

```html
<meta name="viewport" content="width=device-width, initial-scale=1.0">
```

### favicon

在未指定 favicon 时，大多数浏览器会请求 Web Server 根目录下的 favicon.ico 。为了保证 favicon 可访问，避免404，必须遵循以下两种方法之一：

- 在 Web Server 根目录放置 favicon.ico 文件；
- 使用 link 指定 favicon；

```html
<link rel="shortcut icon" href="path/to/favicon.ico">
```