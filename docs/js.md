# JavaScript

## 通用规范

### 注释

- 单行注释

必须独占一行。`//` 后跟一个空格，缩进与下一行被注释说明的代码一致。

- 多行注释

避免使用 `/*...*/` 这样的多行注释。有多行注释内容时，使用多个单行注释。

- 函数/方法注释

  - 函数/方法注释必须包含函数说明，有参数和返回值时必须使用注释标识
  - 参数和返回值注释必须包含类型信息和说明

  ```js
  /**
   * 函数描述
   *
   * @param {string} p1 参数1的说明
   * @param {string} p2 参数2的说明，比较长
   *     那就换行了.
   * @param {number=} p3 参数3的说明（可选）
   * @return {Object} 返回值描述
   */
  function foo(p1, p2, p3) {
      var p3 = p3 || 10;
      return {
          p1: p1,
          p2: p2,
          p3: p3
      };
  }
  ```

### 命名

**变量**, 使用 小驼峰命名法。

```js
var loadingModules = {};
```

**常量**, 使用全部字母大写，单词间下划线分隔的命名方式。

```js
var HTML_ENTITY = {};
```

**函数以及函数的参数，**采用小驼峰命名法。

```js
function stringFormat(source) {}

function hear(theBells) {}
```

**类**, 使用 大驼峰命名法

类的 **方法 / 属性**, 使用 小驼峰命名法

```js
function TextNode(value, engine) {
    this.value = value;
    this.engine = engine;
}

TextNode.prototype.clone = function () {
    return this;
};
```

### 命名语法

**类名**，使用名词。

```js
function Engine(options) {}
```

**函数名**，使用动宾短语。

| 动词   | 含义                 | 返回值                             |
| ---- | ------------------ | ------------------------------- |
| can  | 判断是否可执行某个动作 ( 权限 ) | 函数返回一个布尔值。true：可执行；false：不可执行   |
| has  | 判断是否含有某个值          | 函数返回一个布尔值。true：含有此值；false：不含有此值 |
| is   | 判断是否为某个值           | 函数返回一个布尔值。true：为某个值；false：不为某个值 |
| get  | 获取某个值              | 函数返回一个非布尔值                      |
| set  | 设置某个值              | 无返回值、返回是否设置成功或者返回链式对象           |

```js
//是否可阅读
function canRead(){
    return true;
}

//获取姓名
function getName{
    return this.name
}
```

### 条件(三元)操作符 (?:)

三元操作符用于替代 if 条件判断语句。

```js
// Not recommended
if (val != 0) {
  return foo();
} else {
  return bar();
}

// Recommended
return val ? foo() : bar();
```

### && 和 ||

二元布尔操作符是可短路的, 只有在必要时才会计算到最后一项。

```js
// Not recommended
function foo(opt_win) {
  var win;
  if (opt_win) {
    win = opt_win;
  } else {
    win = window;
  }
  // ...
}

if (node) {
  if (node.kids) {
    if (node.kids[index]) {
      foo(node.kids[index]);
    }
  }
}

// Recommended
function foo(opt_win) {
  var win = opt_win || window;
  // ...
}

var kid = node && node.kids && node.kids[index];
if (kid) {
  foo(kid);
}
```

### 全局命名空间污染与 IIFE

总是将代码包裹成一个 IIFE(Immediately-Invoked Function Expression)，用以创建独立隔绝的定义域。这一举措可防止全局命名空间被污染。

**IIFE（立即执行的函数表达式）**

立即执行的函数表达式的执行括号应该写在外包括号内。虽然写在内还是写在外都是有效的，但写在内使得整个表达式看起来更像一个整体，因此推荐这么做。

```js
// Not recommended
(function(){})();

// Recommended
(function(){}());
```

### 使用严格等

总是使用 `===` 精确的比较操作符，避免在判断的过程中，由 JavaScript 的强制类型转换所造成的困扰

如果你使用 `===` 操作符，那比较的双方必须是同一类型为前提的条件下才会有效。

### 语句块内的函数声明

切勿在语句块内声明函数，在 ECMAScript 5 的严格模式下，这是不合法的。函数声明应该在定义域的顶层。但在语句块内可将函数申明转化为函数表达式赋值给变量。

```js
// Not recommended
if (x) {
  function foo() {}
}

// Recommended
if (x) {
  var foo = function() {};
}
```

### 字符串

统一使用单引号(‘)，不使用双引号(“)。这在创建 HTML 字符串非常有好处：

```js
 var msg = 'This is some HTML <div class="makes-sense"></div>';
```

## jQuery规范

### jQuery 变量

1. 存放 jQuery 对象的变量以 `$` 开头；
2. 将 jQuery 选择器返回的对象缓存到本地变量中复用；
3. 使用驼峰命名变量；

```js
var $myDiv = $("#myDiv");
$myDiv.click(function(){...});
```

### DOM 操作

1. 当要操作 DOM 元素的时候，尽量将其分离节点，操作结束后，再插入节点；
2. 使用字符串连接或 `array.join` 要比 `.append()`性能更好；

```js
// Not recommended
var $myList = $("#list");
for(var i = 0; i < 10000; i++){
    $myList.append("<li>"+i+"</li>");
}

// Recommended
var $myList = $("#list");
var list = "";
for(var i = 0; i < 10000; i++){
    list += "<li>"+i+"</li>";
}
$myList.html(list);

// Much to recommended
var array = [];
for(var i = 0; i < 10000; i++){
    array[i] = "<li>"+i+"</li>";
}
$myList.html(array.join(''));
```

### 事件

1. 如果需要，对事件使用自定义的 `namespace`，这样容易解绑特定的事件，而不会影响到此 DOM 元素的其他事件监听；
2. 对 Ajax 加载的 DOM 元素绑定事件时尽量使用事件委托。事件委托允许在父元素绑定事件，子代元素可以响应事件，也包括 Ajax 加载后添加的子代元素；

```js
// Not recommended
$("#list a").on("click", myClickHandler);

// Recommended
$("#list").on("click", "a", myClickHandler);
```

### 链式写法

1. 尽量使用链式写法而不是用变量缓存或者多次调用选择器方法；
2. 当链式写法超过三次或者因为事件绑定变得复杂后，使用换行和缩进保持代码可读性；

```js
$("#myDiv").addClass("error").show();

$("#myLink")
  .addClass("bold")
  .on("click", myClickHandler)
  .on("mouseover", myMouseOverHandler)
  .show();
```